import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
public class MDI extends JFrame {

    private static final long serialVersionUID = 1L;

    private JPanel desktopPane = new JPanel();

    public MDI() {
        setTitle("MDI ");
        setSize(new Dimension(800, 600));
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new BorderLayout());

        JMenuBar menuBar = new JMenuBar();
        JMenu fileMenu = new JMenu("File");
        JMenuItem newItem = new JMenuItem("New");
        JMenu editMenu = new JMenu("Edit");
        JMenuItem newedit = new JMenuItem("New");
        fileMenu.add(newItem);
        editMenu.add(editMenu);
        
        menuBar.add(fileMenu);
        setJMenuBar(menuBar);

        desktopPane.setLayout(new BorderLayout());
        add(desktopPane, BorderLayout.CENTER);

        newItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JInternalFrame frame = new JInternalFrame("New Internal Frame", true, true, true, true);
                frame.setSize(new Dimension(200, 150));
                frame.setLocation(desktopPane.getComponentCount() * 10, desktopPane.getComponentCount() * 10);
                frame.setVisible(true);
                desktopPane.add(frame);
            }
        });
    }

    public static void main(String[] args) {
        MDI mdi = new MDI();
        mdi.setVisible(true);
    }
}
