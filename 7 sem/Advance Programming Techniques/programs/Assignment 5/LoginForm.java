import java.awt.event.*;
import java.awt.*;
import javax.swing.*;

public class LoginForm extends JFrame {

    JLabel lblUserName;
    JLabel lblPassword;
    JTextField txtUsername;
    JPasswordField txtPassword;
    JButton btnLogin;
    final JFrame frame = this;
    // init code
    public LoginForm(){
        // components init
        this.setTitle("Login");
        this.lblUserName = new JLabel("User name");
        lblPassword = new JLabel();
        lblPassword.setText("Password");
        txtUsername = new JTextField();
        txtPassword = new JPasswordField();
        btnLogin = new JButton("Login");
        btnLogin.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent event){
                String username = txtUsername.getText();
                String password = new String(txtPassword.getPassword());
                // validation
                if(username.equals("") || password.equals("")){
                    JOptionPane.showMessageDialog(
                        new JFrame(), 
                        "Username and password should be provided", 
                        "Validation",
                        JOptionPane.ERROR_MESSAGE
                    );
                }
                System.out.println("password is: "+password);

                LoginProcess logingProcess = new LoginProcess();
                boolean isLoggedIn= logingProcess.doLogin(username, password);
                if(isLoggedIn){
                    System.out.println("Logged in");
                    JOptionPane.showMessageDialog(frame, "Logged in", "Dialog" ,JOptionPane.INFORMATION_MESSAGE);
                    JFrame form = new JFrame();
                    form.setSize(400,400);
                    form.setVisible(true);
                }
                else{
                    JOptionPane.showMessageDialog(frame, "Username or password invalid", "Dialog" ,JOptionPane.ERROR_MESSAGE);
                    System.out.println("Not logged in");
                }
            }
        });
        // renderd
        this.setSize(200,200);
        this.setLayout(new GridLayout(0,2));
        this.add(lblUserName);
        this.add(txtUsername);
        this.add(lblPassword);
        this.add(txtPassword);
        this.add(btnLogin);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // show
        this.setVisible(true);
    }
}
