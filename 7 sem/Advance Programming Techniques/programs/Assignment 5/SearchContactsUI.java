import java.awt.*;
import java.awt.event.*;
import java.sql.*;
import javax.swing.*;

public class SearchContactsUI extends JFrame {
  private JLabel usernameLabel;
  private JTextField usernameTextField;
  private JLabel sidLabel;
  private JTextField sidTextField;
  private JButton submitButton;

  public SearchContactsUI() {
    super("Search Contacts");
    setLayout(new FlowLayout());
    JPanel searchPanel = new JPanel();
    searchPanel.setLayout(new GridLayout(2, 2));
    usernameLabel = new JLabel("username:");
    searchPanel.add(usernameLabel);
    usernameTextField = new JTextField(20);
    searchPanel.add(usernameTextField);
    sidLabel = new JLabel("SID:");
    searchPanel.add(sidLabel);
    sidTextField = new JTextField(20);
    searchPanel.add(sidTextField);
    add(searchPanel);
    submitButton = new JButton("Submit");
    add(submitButton);
    submitButton.addActionListener(new ActionListener() {
      
      public void actionPerformed(ActionEvent e) {
        String username = usernameTextField.getText();
        String sid = sidTextField.getText();
        searchFromDatabase(username, sid);
      }
    });
    setSize(300, 150);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  private void searchFromDatabase(String username, String sid) {
    try {
      Class.forName("org.sqlite.JDBC");
      Connection connection = DriverManager.getConnection("jdbc:sqlite:db2.db");
      Statement statement = connection.createStatement();
      ResultSet resultSet = statement.executeQuery("SELECT username, password, sid FROM login WHERE username LIKE '%" + username + "%'  AND sid LIKE '%" + sid + "%'");
      StringBuilder result = new StringBuilder();
      while (resultSet.next()) {
        result.append("Username: ").append(resultSet.getString("username")).append(", Password: ").append(resultSet.getString("password")).append(", SID: ").append(resultSet.getString("sid")).append("\n");
      }
      resultSet.close();
      statement.close();
      connection.close();
      JOptionPane.showMessageDialog(this, result.toString(), "Search Result", JOptionPane.INFORMATION_MESSAGE);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    new SearchContactsUI();
  }
}



