import java.awt.*;
import java.sql.*;
import javax.swing.*;
import java.awt.event.*;

public class AddContactUI extends JFrame {
  private JLabel usernameLabel;
  private JTextField usernameTextField;
  private JLabel passwordLabel;
  private JTextField passwordTextField;
  private JLabel sidLabel;
  private JTextField sidTextField;
  
  private JButton submitButton;

  public AddContactUI() {
    super("Add Contact");
    setLayout(new FlowLayout());
    usernameLabel = new JLabel("username:");
    add(usernameLabel);
    usernameTextField = new JTextField(20);
    add(usernameTextField);
    passwordLabel = new JLabel("password:");
    add(passwordLabel);
    passwordTextField = new JTextField(20);
    add(passwordTextField);
    sidLabel = new JLabel("Sid:");
    add(sidLabel);
    sidTextField = new JTextField(20);
    add(sidTextField);
    submitButton = new JButton("Submit");
    add(submitButton);
    submitButton.addActionListener(new ActionListener() {
      
      public void actionPerformed(ActionEvent e) {
        String username = usernameTextField.getText();
        String password = passwordTextField.getText();
        String sid = sidTextField.getText();
        saveToDatabase(username, password, sid);
      }
    });
    setSize(400, 300);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setVisible(true);
  }

  private void saveToDatabase(String username, String password, String sid ) {
    try {
      Class.forName("org.sqlite.JDBC");
      Connection connection = DriverManager.getConnection("jdbc:sqlite:db2.db");
      PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO login (username, password, sid) VALUES (?, ?, ?)");
      preparedStatement.setString(1, username);
      preparedStatement.setString(2, password);
      preparedStatement.setString(3, sid);
      preparedStatement.executeUpdate();
      preparedStatement.close();
      connection.close();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void main(String[] args) {
    new AddContactUI();
  }
}