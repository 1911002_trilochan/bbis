import java.sql.*;

import javax.naming.spi.DirStateFactory.Result;
import javax.sql.StatementEvent;

public class LoginProcess {
    public boolean doLogin(String username,String password){
        // connection
        String connString ="jdbc:sqlite:db2.db";
        Connection conn ;
        try{ 
            conn = DriverManager.getConnection(connString);
            // execute
            String sql = "select password, sid from login where username = '"+username+"';";
            Statement statement = conn.createStatement();
            ResultSet results = statement.executeQuery(sql);
            if(results.next()){
                // got a result
                String dbPwd = results.getString("password");
                System.out.println("password from db : "+dbPwd);
                if(dbPwd.equals(password)) return true;
                else return false;
            }
            else{
                // no result found
                System.out.println("Username not found");
                return false;
            }
        }catch(Exception ex){
            System.out.println(ex.getMessage());
            return false;
        }
    }
}
