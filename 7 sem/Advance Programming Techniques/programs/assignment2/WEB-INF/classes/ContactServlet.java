import java.io.*;
import javax.servlet.http.*;
import java.sql.*;


public class ContactServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response) 
        throws IOException {

            // Set response content type
            response.setContentType("text/html");
            PrintWriter out = response.getWriter();
    
            try {
                
                Class.forName("org.sqlite.JDBC");
    
                
                Connection conn = DriverManager.getConnection("jdbc:sqlite:C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\assignment2\\Contact.db");
    
                
                Statement stmt = conn.createStatement();
                String sql = "SELECT * FROM contacts";
                ResultSet rs = stmt.executeQuery(sql);
    
                
                out.println("<html><body><table border='1'>");
                out.println("<tr> <th>cid</th> <th>name</th> <th>email</th></tr>");
                while (rs.next()) {
                    int cid = rs.getInt("cid");
                    String name = rs.getString("name");
                    String email = rs.getString("email");
    
                   
                    out.println("<tr><td>" + cid + "</td><td>" + name + "</td><td>" + email + "</td></tr>");
                }
                out.println("</table></body></html>");
    
                
                rs.close();
                stmt.close();
                conn.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
}
