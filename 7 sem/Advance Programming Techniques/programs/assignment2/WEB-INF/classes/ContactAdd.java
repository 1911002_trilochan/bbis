import java.io.*;
import java.sql.*;
import javax.servlet.http.*;

public class ContactAdd extends HttpServlet {

    public void doPost (HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        String i = request.getParameter("cid");
        int cid = Integer.parseInt(i);
        String name = request.getParameter("name");
        String email = request.getParameter("email");

        try {
            // Register JDBC driver
            Class.forName("org.sqlite.JDBC");

            // Open a connection
            Connection conn = DriverManager.getConnection("jdbc:sqlite:C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\assignment2\\Contact.db");

            // Execute SQL query
            String sql = "INSERT INTO Contacts (cid, name, email) VALUES (?, ?, ?)";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, cid);
            stmt.setString(2, name);
            stmt.setString(3, email);
            stmt.executeUpdate();

            // Clean-up environment
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("data");
    }
}