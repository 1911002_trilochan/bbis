import java.io.*;
import java.sql.*;
import javax.servlet.http.*;

public class ContactSearch extends HttpServlet {
    public void doPost (HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        response.setContentType("text/html");
        PrintWriter out = response.getWriter();
        
        String formName = request.getParameter("formName");
        String formEmail = request.getParameter("formEmail");
        
        try {
            Class.forName("org.sqlite.JDBC");

            Connection conn = DriverManager.getConnection("jdbc:sqlite:C:\\Program Files\\Apache Software Foundation\\Tomcat 9.0\\webapps\\assignment2\\Contact.db");

            Statement stmt = conn.createStatement();
            String sql = "SELECT * FROM contacts WHERE name LIKE '" + formName + "' AND email LIKE '" + formEmail + "'";
            ResultSet rs = stmt.executeQuery(sql);

            out.println("<html><body><table border='1'>");
            out.println("<tr><th>CID</th><th>Name</th><th>Email</th></tr>");
            while (rs.next()) {
                int cid = rs.getInt("cid");
                String name = rs.getString("name");
                String email = rs.getString("email");

                out.println("<tr><td>" + cid + "</td><td>" + name + "</td><td>" + email + "</td></tr>");
            }
            out.println("</table></body></html>");

            rs.close();
            stmt.close();
            conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}