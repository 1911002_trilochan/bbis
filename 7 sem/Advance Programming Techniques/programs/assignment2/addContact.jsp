<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<title>Add Contact</title>
</head>
<body>
	<h1>Add a New Contact</h1>
	<form action="addContact" method="post">
		<label for="cid">CID:</label><br>
		<input type="number" id="cid" name="cid"><br>
        <label for="name">Name:</label><br>
		<input type="text" id="name" name="name"><br>
		<label for="email">Email:</label><br>
		<input type="email" id="email" name="email"><br>
		<br>
		<input type="submit" value="Submit">
	</form> 
</body>
</html>
