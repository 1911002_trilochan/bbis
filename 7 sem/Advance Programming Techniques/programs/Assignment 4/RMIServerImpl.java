import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class RMIServerImpl extends UnicastRemoteObject implements RMIService {
    protected RMIServerImpl() throws RemoteException {
        super();
    }


    public long calculateFactorial(int number) throws RemoteException {
        long result = 1;
        for (int i = 1; i <= number; i++) {
            result *= i;
        }
        return result;
    }
}