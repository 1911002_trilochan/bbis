import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Scanner;

public class RMIClient {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter the number to calculate its factorial: ");
            int number = sc.nextInt();
            RMIService service = (RMIService) Naming.lookup("rmi://localhost:1099/factorial-service-0");
            long result = service.calculateFactorial(number);
            System.out.println("Factorial of " + number + " is " + result);
        } catch (Exception e) {
            System.out.println("FactorialClient exception: " + e);
        }
    }
}




