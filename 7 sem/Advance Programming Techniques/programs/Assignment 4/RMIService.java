import java.rmi.Remote;
import java.rmi.RemoteException;

public interface RMIService extends Remote {
    long calculateFactorial(int number) throws RemoteException;
}