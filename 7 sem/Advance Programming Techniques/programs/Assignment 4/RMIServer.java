
import java.rmi.Naming;
import java.rmi.RemoteException;
import java.util.Scanner;

public class RMIServer {
    public static void main(String[] args) {
        try {
            Scanner sc = new Scanner(System.in);
            System.out.print("Enter the number of servers: ");
            int numServers = sc.nextInt();
            for (int i = 0; i < numServers; i++) {
                RMIService service = new RMIServerImpl();
                Naming.rebind("rmi://localhost:1099/factorial-service-" + i, service);
                System.out.println("FactorialServer " + i + " is ready.");
            }
        } catch (Exception e) {
            System.out.println("FactorialServer failed: " + e);
        }
    }
}





