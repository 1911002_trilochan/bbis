import java.awt.*;
import javax.swing.*;

public class Animate extends JPanel {
    int x = 0;
    int y = 0;

    private void moveBall(){
        this.x = this.x+1;
        this.y = this.x+1;
    }

    public void paint(Graphics g){
        super.paint(g);
        g.fillOval(x, y, 30, 30);

    }

    public static void main (String[] args)
        throws InterruptedException{
        JFrame frame = new JFrame("Main Frame");
        Animate animate = new Animate();
        frame.add(animate);
        frame.setSize(600, 400);
        frame.setVisible(true);
        Thread.sleep(2000);
        while(true){
            animate.moveBall();
            animate.repaint();
            Thread.sleep(100);
        }
    }
}

