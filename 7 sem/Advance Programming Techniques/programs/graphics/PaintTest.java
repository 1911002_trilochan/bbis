import javax.swing.*;
import java.awt.*;

public class PaintTest extends JPanel {
    public void paintComponent(Graphics g){
        super.paintComponent(g);
        this.setBackground(Color.BLUE);
        //Graphic draw
        g.setColor(Color.white);
        g.fillRect(15, 25, 100, 20);
        g.drawString("This is white", 120, 40);
    }
    public static void main (String args[]){
        JFrame frame = new JFrame("Paint");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        PaintTest paintTest = new PaintTest();
        frame.add(paintTest);
        frame.setSize(200,200);
        frame.setVisible(true);
    }
}