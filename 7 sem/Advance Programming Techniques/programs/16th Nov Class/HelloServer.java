import java.io.*;
import java.net.*;

public class HelloServer extends Thread {
    public ServerSocket serverSocket;

    public HelloServer() throws IOException{
        serverSocket = new ServerSocket(8080);
    }
    
    public void run(){
        while(true){ 
            try{ 
                Socket clientSocket = serverSocket.accept();
                DataInputStream in = new DataInputStream( 
                    clientSocket.getInputStream()
                );
                String inputFromClient = in.readUTF();
                System.out.println("Data From Client:" + inputFromClient);

                DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
                outToClient.writeUTF("PONG");


            } catch (IOException exp){
                // exp.printStackTrace();
                System.out.println("run: "+ exp.getMessage());
            }
            
        }
    }
    public static void main(String[] args) {
        try{ 
            Thread serverThread = new HelloServer();
            serverThread.start();
        }catch (IOException exp){
            System.out.println("MAIN: "+exp.getMessage());
        }
    }

}