import java.net.*;
import java.io.*;


public class HelloUDPServer {
    public static void main(String[] args) throws Exception {
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];

        DatagramSocket serveSocket = new DatagramSocket(8090);
        while (true){
            // receive
            DatagramPacket receivePacket 
                = new DatagramPacket(receiveData, receiveData.length);
            serveSocket.receive(receivePacket);
            String receivedDataString = new String( receiveData);
            System.out.println("Data from Client: "+ receivedDataString);


            // send
            InetAddress clientAddress = receivePacket.getAddress();
            int clientPort = receivePacket.getPort();

            sendData = "PONG".getBytes();
            DatagramPacket sendPacket = new DatagramPacket(
                sendData, sendData.length, clientAddress, clientPort
            );
            serveSocket.send(sendPacket);
        }
    }
}