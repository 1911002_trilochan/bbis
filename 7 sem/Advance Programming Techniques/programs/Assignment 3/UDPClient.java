import java.net.*;
import java.io.*;

public class UDPClient {
    public static void main(String[] args) throws Exception {
        byte[] receiveData = new byte[1024];
        byte[] sendData = new byte[1024];

        InetAddress serverAddress = InetAddress.getByName("127.0.0.1");
        int port = 8090;

        // send data
        DatagramSocket clientSocket = new DatagramSocket();
        sendData = "PING".getBytes();
        DatagramPacket sendPacket = new DatagramPacket(
            sendData, sendData.length, serverAddress, port
        );
        clientSocket.send(sendPacket);

        // receive data
        DatagramPacket receivePacket = new DatagramPacket(
            receiveData, receiveData.length
        );
        clientSocket.receive(receivePacket);
        String receivedString = new String(receiveData);
        System.out.println("Data from Server: " + receivedString);

        

    }
}