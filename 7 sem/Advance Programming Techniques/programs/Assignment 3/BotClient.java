import java.io.*;
import java.net.*;
import java.util.Scanner;

public class BotClient{
    public static void main(String[] args) throws IOException{
        Socket clienSocket = new Socket("localhost", 8080);
        BufferedReader in = new BufferedReader(new InputStreamReader(clienSocket.getInputStream()));
        PrintWriter out = new PrintWriter(clienSocket.getOutputStream(), true);
        Scanner scanner = new Scanner(System.in);
        String inputdata;
        System.out.println("Welcome to the chatbot!!!");
        System.out.println("To know the weather: Enter Weather");
        System.out.println("To know the developer of the program: Enter Developer");
        System.out.println("To know the location : Enter Location");
        System.out.println("To know port no.: Enter Port");
        System.out.println("To know the PC company: Enter Company");
        System.out.println("To know the windows version: Enter Version");
        System.out.println("To know the RAM size: Enter RAM");
        while (true){

            System.out.print("Enter from the menu: ");
            inputdata = scanner.nextLine();
            out.println(inputdata);
            System.out.println("Server Message: " + in.readLine());
        }
    }
}