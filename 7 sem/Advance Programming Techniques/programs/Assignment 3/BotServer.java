import java.io.*;
import java.net.*;

public class BotServer{
    public static void main(String[] args) throws IOException {
        ServerSocket serverSocket = new ServerSocket(8080);
        while(true){
            Socket clientSocket = serverSocket.accept();
            BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
            PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
            String lineInput;
            while((lineInput = in.readLine()) != null){
                if (lineInput.equals("PING")){
                    out.println("PONG");
                }else{
                    out.println(getResponse(lineInput));
                }
            }
        }
    }

    //Chatbot function
    public static String getResponse(String inputdata){

        if (inputdata.contains("Weather")){
            return "The weather is wonderfull today.";
        }else if (inputdata.contains("Developer")){
            return "Trilochan Adhikari";
        }else if (inputdata.contains("Location")){
            return "Kathmandu, Nepal";
        }else if (inputdata.contains("Port")){
            return "8080";
        }else if (inputdata.contains("Company")){
            return "Lenavo";
        }else if (inputdata.contains("Version")){
            return "11";
        }else if (inputdata.contains("RAM")){
            return "8 GB";
        }else {
            return "Sorry Could not reply";
        }
    }
}