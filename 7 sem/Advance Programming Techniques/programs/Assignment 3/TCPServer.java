import java.io.*;
import java.net.*;

public class TCPServer extends Thread {
    public ServerSocket serverSocket;

    public TCPServer() throws IOException{
        serverSocket = new ServerSocket(8080);
    }
    public void run(){
        while(true){ 
            try{ 
                Socket clientSocket = serverSocket.accept();
                DataInputStream in = new DataInputStream( 
                    clientSocket.getInputStream()
                );
                String inputFromClient = in.readUTF();
                System.out.println("Data From Client:" + inputFromClient);

                DataOutputStream outToClient = new DataOutputStream(clientSocket.getOutputStream());
                outToClient.writeUTF("PONG");

            } catch (IOException exp){
                System.out.println("run: "+ exp.getMessage());
            }
        }
    }
    public static void main(String[] args) {
        try{ 
            Thread serverThread = new TCPServer();
            serverThread.start();
        }catch (IOException exp){
            System.out.println("MAIN: "+exp.getMessage());
        }
    }
}