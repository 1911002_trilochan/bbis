import java.io.*;
import java.net.*;

public class TCPClient {
    public static void main(String[] args){
        // server address and port
        String hostname = "127.0.0.1"; int port = 8080;
        try{ 
            Socket client = new Socket(hostname, port);
            OutputStream outToServer = client.getOutputStream();
            DataOutputStream out = new DataOutputStream(outToServer);
           
            out.writeUTF("PING");

            // read data from server
            InputStream readFromServer = client.getInputStream();
            DataInputStream in = new DataInputStream(readFromServer);
            String dataFromServer = in.readUTF();
            System.out.println("Data from Server: " + dataFromServer);

            client.close();
        } catch(Exception exp){
            exp.printStackTrace();
        }
    }
}