import java.sql.*;

import javax.print.PrintService;

public class JDBCTest{
    public static void main(String[] args) {
        try{ 
            Class.forName("org.sqlite.JDBC");
            Connection conn = DriverManager.getConnection("jdbc:sqlite:JavaTestDB.db");
            System.out.println("Database connection made");
            // read data
            Statement stmt = conn.createStatement();
            String sql = "select * from student";
            ResultSet rs = stmt.executeQuery(sql);
            rs.next();
            String name = rs.getString("name");
            System.out.println("Student Name: "+ name);
// javac *.java && java -classpath ".:sqlite-jdbc-3.39.3.0.jar" JDBCTest
        } catch(Exception ex){
            System.out.println("Error: "+ ex.getMessage());

            
        }
    }
}