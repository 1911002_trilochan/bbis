import java.sql.*;
import javax.swing.ViewportLayout;

public class contacts{
    private String cid;
    private String name;
    private String email;

    //Constructor Made
    contacts(String _Cid, String _Name, String _Email){
        this.cid= _Cid;
        this.name= _Name;
        this.email= _Email;
    }

    //Using Setter
    public void setcid(String _Cid){this.cid=_Cid;}
    public void setname(String _Name){this.name=_Name;}
    public void setEmail(String _Email){this.email=_Email;}

    //Using Getter
    public String getcid(){return this.cid;}
    public String getname(){return this.name;}
    public String getemail(){return this.email;}

    //Function
    public void Save() throws Exception{
        Class.forName("org.sqlite.JDBC");
        Connection conn = DriverManager.getConnection("jdbc:sqlite:Contacts.db");
        Statement stmt= conn.createStatement(); 

        String sql= "Insert into contacts values('"+this.cid+"', '"+this.name+"', '"+this.email+"')";
        stmt.executeUpdate(sql);
    }

}