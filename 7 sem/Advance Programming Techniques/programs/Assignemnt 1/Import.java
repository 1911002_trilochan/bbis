import java.io.File;
import java.io.FileNotFoundException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Scanner;

public class Import {
    public static void main(String[] args){

        //Connection with database

        try{
            Class.forName("org.sqlite.JDBC");
            Connection con= DriverManager.getConnection("jdbc:sqlite:Contacts.db");
            System.out.println("DataBase Connection Made");

        }catch(Exception e){
            System.out.println("An error occurred");
            e.printStackTrace();    
        }

        //Reading Contact.csv file

        try{
            final File myObj= new File("contacts.csv"); 
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()){
                String data = myReader.nextLine();
                System.out.println(data);
            }
            myReader.close();
        }catch (FileNotFoundException e){
            System.out.println("An error occurred");
            e.printStackTrace();
        }

    }
    
}
