import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class FlowLayoutTest extends JFrame{
    JButton btn1, btn2, btn3;
    FlowLayoutTest(){
        super();
        InitUI();
    }
    private void InitUI(){
        setTitle("Flow Layout Test");
        setLayout(new FlowLayout());
        //close event
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // init the controls
        btn1 = new JButton("First");
        btn2 = new JButton("Second");
        btn3 = new JButton("Third");

        add(btn1);
        add(btn2);
        add(btn3);

        setSize(400,400);
    }

    public static void main(String[] args){
        FlowLayoutTest flowLayoutTest = new FlowLayoutTest();
        flowLayoutTest.setVisible(true);
    }
}