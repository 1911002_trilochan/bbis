import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BoxLayoutTest extends JFrame{
    JButton btn1, btn2, btn3;
    BoxLayoutTest(){
        super();
        InitUI();
    }
    private void InitUI(){
        //box layout => new BoxLayout(container, axis)
        //get the content panel of frame => this.getContentPane()
        setTitle("Box Layout Test");
        setLayout(new BoxLayout(this.getContentPane(), BoxLayout.Y_AXIS));
        //close event
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        // init the controls
        btn1 = new JButton("First");
        btn2 = new JButton("Second");
        btn3 = new JButton("Third");

        add(btn1);
        add(btn2);
        add(btn3);

        setSize(400,400);
    }

    public static void main(String[] args){
        BoxLayoutTest boxLayoutTest = new BoxLayoutTest();
        boxLayoutTest.setVisible(true);
    }
}