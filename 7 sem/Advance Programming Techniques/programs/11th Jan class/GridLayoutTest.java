import java.awt.*;
import javax.swing.*;


public class GridLayoutTest extends JFrame{

    JLabel lblUserName, lblPassword;
    JTextField txtUserName;
    JPasswordField txtPassword;
    JButton btnSubmit, btnCancel;

    GridLayoutTest(){
        super();
        InitUI();
    }

private void InitUI(){
    setTitle("Grid Layout Test");
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    setLayout(new GridLayout(6,1));

    //init the controls
    lblUserName = new JLabel("User name");
    lblPassword = new JLabel("Password");
    txtUserName = new JTextField();
    txtPassword = new JPasswordField();
    btnSubmit = new JButton("Log In");
    btnCancel = new JButton("Cancel");

    //add to frame
    add(lblUserName);
    add(txtUserName);
    add(lblPassword);
    add(txtPassword);
    add(btnSubmit);
    add(btnCancel);
    pack();
    }
    public static void main(String[] args){
        new GridLayoutTest().setVisible(true);
    }
}