import java.rmi.*;

public class RmiClient {
    public static void main(String[] args) {
        try{ 
            IAdder adderRemoteObj ;
            adderRemoteObj = (IAdder)Naming.lookup("rmi://localhost:5000/Adder");
            int sum = adderRemoteObj.add(10,20);
            System.out.println("The sum from remote object = "+ sum);
        } catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }    
}
