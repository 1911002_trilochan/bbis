import java.rmi.*;

public class RmiServer {
    public static void main(String[] args) {
        try {
            IAdder adder = new Adder();
            Naming.rebind(
                "rmi://localhost:5000/Adder", 
                adder
            );
        } catch(Exception ex){
            System.out.println(ex.getMessage());
        }
    }
}
