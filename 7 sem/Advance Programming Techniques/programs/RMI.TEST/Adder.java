import java.rmi.*;
import java.rmi.server.*;

public class Adder 
    extends UnicastRemoteObject
    implements IAdder{
    
        public Adder() throws RemoteException{
            super();
        }

        public int add(int x, int y){
            return x + y;
        }
}
