import javax.servlet.http.*;
import java.io.*;

public class HelloServlet extends HttpServlet{
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        PrintWriter pw = response.getWriter();
        pw.println("Hello Servlet from BBIS-7th-APT-2022 Fall");
        pw.close();
    }
    
}