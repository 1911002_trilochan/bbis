import java.awt.*;
import java.awt.event.*;

public class LoginFrame extends Frame{
    //control
    Label lbl; 
    Button btnLogin;
    TextField txtUserName, txtPassword;

    LoginFrame(){
        super();
        setSize(320,220);
        initUI();
    }
    private void initUI(){
        lbl = new Label("Provide Username to Login");
        lbl.setBounds(30, 30, 300, 30);
        add(lbl);

        txtUserName = new TextField();
        txtUserName.setBounds(30, 70, 200, 30);
        add(txtUserName);

        txtPassword = new TextField();
        txtPassword.setBounds(30, 110, 200, 30);
        txtPassword.setEchoChar('*');
        add(txtPassword);

        btnLogin = new Button("Login");
        btnLogin.setBounds(30, 140, 100, 30);
        add(btnLogin);

        addWindowListener(
            new WindowAdapter(){
                public void windowClosing(WindowEvent windowEvent){
                    System.exit(0);
                }
            }  
        );

        setLayout(null);

    }

    public void showLogin(){
        setVisible(true);
    }
}