import java.awt.*;
import java.awt.event.*;

public class FrameTest{
    public static void main(String[] args) {
        Frame windowsFrame = new Frame();
        windowsFrame.setSize(500,500);
        windowsFrame.addWindowListener(
            new WindowAdapter(){
                public void windowClosing(WindowEvent windowEvent){
                    System.exit(0);
                }
            }
        );
        // adding controls
        Label lbl = new Label("Input your name");
        lbl.setBounds(30, 50, 180, 30);
        windowsFrame.add(lbl);

        TextField txt = new TextField();
        txt.setBounds(30, 100, 180, 30);
        windowsFrame.add(txt);

        Button btn = new Button("save");
        btn.setBounds(30, 150, 80, 30);
        windowsFrame.add(btn);

        windowsFrame.setLayout(null);
        windowsFrame.setVisible(true);


        windowsFrame.setVisible(true);
    }
}